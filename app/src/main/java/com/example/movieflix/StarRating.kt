package com.example.movieflix

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun StarRating(modifier: Modifier = Modifier, rating: Double) {
	val filledStars = rating.toInt()
	val hollowStars = (5.0 - rating).toInt()
	
	Row(modifier) {
		repeat(filledStars) {
			Icon(imageVector = Icons.Default.Star, contentDescription = null, tint = Color(0xFFFFDC2F))
		}
		
		if(filledStars + hollowStars != 5) {
			//TODO: Use half star
			Icon(imageVector = Icons.Default.Star, contentDescription = null, tint = Color(0xFFFFDC2F))
		}
		
		repeat(hollowStars) {
			//TODO: Use hollow star
			Icon(imageVector = Icons.Default.Star, contentDescription = null, tint = Color(0xFF000000))
			
		}
	}
}

@Preview
@Composable
fun StarRatingPreview() {
	Column {
		StarRating(rating = 1.0)
		StarRating(rating = 2.0)
		StarRating(rating = 3.0)
		StarRating(rating = 4.0)
		StarRating(rating = 5.0)
	}
}