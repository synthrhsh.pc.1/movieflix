package com.example.movieflix

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter

data class MovieCardWideDisplayItem(
	val imageUrl: String,
	val titleText: String,
	val favorite: Boolean,
	val rating: Double,
	val dateText: String,
	val onFavoriteChange: (Boolean) -> Unit
)

@Composable
fun MovieCardWide(modifier: Modifier = Modifier, displayItem: MovieCardWideDisplayItem) {
	val ratio = 1.92f
	
	val painter = rememberAsyncImagePainter(model = displayItem.imageUrl)
	
	val outerCornerRadius = 24.dp
	val borderWidth = 6.dp
	val innerCornerRadius = outerCornerRadius - borderWidth
	
	
	Box(
		modifier
			.aspectRatio(ratio)
			.clip(RoundedCornerShape(outerCornerRadius))
			.background(Color(0xFFF3B73F))
			.padding(borderWidth)
			.clip(RoundedCornerShape(innerCornerRadius))
			.background(Color.White)
	) {
		Image(
			modifier = Modifier.fillMaxSize(),
			painter = painter,
			contentScale = ContentScale.FillBounds,
			contentDescription = null
		)
		
		Column(
			modifier = Modifier
				.fillMaxSize()
				.background(brush = Brush.verticalGradient(colors = listOf(Color.Transparent, Color.Black)))
				.padding(horizontal = 12.dp)
			,
			verticalArrangement = Arrangement.Bottom
		) {
			//TODO: Add marquee
			Text(text = displayItem.titleText, color = Color.White, style = MaterialTheme.typography.titleLarge)
			
			Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
				StarRating(rating = displayItem.rating)
				
				Spacer(modifier = Modifier.width(8.dp))
				
				Text(text = displayItem.dateText, color = Color.White, style = MaterialTheme.typography.bodyLarge)
				
				Spacer(modifier = Modifier.weight(1f))
				
				IconButton(onClick = { displayItem.onFavoriteChange(!displayItem.favorite) }) {
					val tint = if (displayItem.favorite)
						Color(0xFFFF4857)
					else
						Color(0xDDDDDDDD)
					
					Icon(imageVector = Icons.Default.Favorite, contentDescription = null, tint = tint)
				}
			}
		}
	}
}

@Preview(showBackground = true)
@Composable
fun MovieCardWidePreview() {
	var favoriteState by remember { mutableStateOf(false) }
	
	MovieCardWide(
		modifier = Modifier
			.fillMaxWidth()
			.padding(16.dp),
		displayItem = MovieCardWideDisplayItem(
			imageUrl = "https://cdn.mos.cms.futurecdn.net/N3XRjHiKddRBC2ZqdmbuYU-650-80.jpg.webp",
			titleText = "Avatar: the massive fuckup",
			favorite = favoriteState,
			rating = 3.5,
			dateText = "4/4/2024",
			onFavoriteChange = { favoriteState = it }
		)
	)
}
