package com.example.movieflix

import android.widget.ImageButton
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

data class MovieDisplayItem(
	val title: String,
	val imageUrl: String,
	val genresText: String,
	val favorite: Boolean,
	val releaseDate: String,
	val runtime: String,
	val description: String,
	val reviewDisplayItems: List<ReviewDisplayItem>
)

data class ReviewDisplayItem(
	val userName: String,
	val text: String
)

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieDetails() {
	val ratio = 376 / 214
	
	Scaffold(Modifier.fillMaxSize(), topBar = { TopBar {} }) { scaffoldPadding ->
		//TODO: Make scrollable
		Column(
			modifier = Modifier
				.padding(scaffoldPadding)
				.fillMaxSize()
		) {
			
		}
	}
}

@Composable
fun TopBar(onBackClick: () -> Unit) {
	Row(
		modifier = Modifier.fillMaxWidth(),
		horizontalArrangement = Arrangement.Start,
		verticalAlignment = Alignment.CenterVertically
	) {
		IconButton(onClick = onBackClick) {
			//TODO: Make tint automatic
			Icon(imageVector = Icons.Default.ArrowBack, contentDescription = null, tint = Color.Black)
		}
	}
}

@Preview
@Composable
fun MovieDetailsPreview() {
	MovieDetails()
}
